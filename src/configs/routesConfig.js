/* eslint-disable react-hooks/exhaustive-deps */

// packages
import React, { useEffect } from "react";
import { Routes, Route, useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
// redux imports
import { accessSelectore } from "../store/auth/selector";
import { setRouteDetails } from "../store/ui";
// consts
import routeDetails from "../consts/routesDetails";
import { innerRoutes, privateRoutes } from "../consts/authLocations";
// pages
import { Home, Login, PNF, Layout } from "../pages";

const RoutesBase = () => {
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const access = useSelector(accessSelectore);
  const checker = !!access ? true : false;

  useEffect(() => {
    // token validate
    if (innerRoutes.includes(pathname)) {
      if (!checker) {
        navigate("/login", { replace: true });
      }
    } else if (privateRoutes.includes(pathname)) {
      if (checker) {
        navigate("/", { replace: true });
      }
    }
  });

  useEffect(() => {
    // set current route details
    dispatch(setRouteDetails(routeDetails[pathname]));
  }, [pathname]);

  // routes
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<PNF />} />
      </Routes>
    </Layout>
  );
};

export default RoutesBase;
