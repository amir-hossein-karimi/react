import { createTheme, ThemeProvider as DefaultTheme } from "@mui/material/styles";
import {theme as themeOptions} from "../consts/theme";

const theme = createTheme(themeOptions)

const ThemeProvider = (props) => {
    return <DefaultTheme {...props}>{props.children}</DefaultTheme>;
}

export { theme, ThemeProvider }