import React from "react"
import { BrowserRouter } from "react-router-dom"
import RoutesBase from "./configs/routesConfig";

const App = () => {

  return (
    <>
      <BrowserRouter>
        <RoutesBase />
      </BrowserRouter>
    </>
  );
}

export default App;
