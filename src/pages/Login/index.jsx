import React from 'react';
import { useDispatch } from 'react-redux';
import { setAccess } from "../../store/auth"

const Login = () => {
    const dispatch = useDispatch()

    const handleToken = () => {
        dispatch(setAccess('test token'))
    }
    return (
        <div>
            login

            <button onClick={handleToken}>handleToken</button>
        </div>
    );
};

export default Login;