import React from 'react';
import { useDispatch } from 'react-redux';
import { emptyAccess } from "../../store/auth"
import { makeStyles } from '@mui/styles';
import { useForm, yup } from "../../hooks/useForm"

const schema = yup.object().shape({
    name: yup.string().required('این فیلد اجباریست').max(5),
    age: yup.number().required('اجباریع اقا').min(2).typeError('عدد بزن').positive('مثبت')
})

const useStyles = makeStyles((theme) => ({
    root: {
      color: theme.test,
    }
  }));

const Home = () => {
    const { register, handleSubmit, formState: { errors } } = useForm({
        schema
    })
    const classes = useStyles();
    const dispatch = useDispatch()

    const handleEmpty = () => {
        dispatch(emptyAccess())
    }

    return (
        <div>
            <form onSubmit={handleSubmit(d => { console.log(d) })}>
                <input {...register('name')} />
                {errors?.name?.message}
                <input {...register('age')} />
                {errors?.age?.message}
                <button type='submit'>send</button>
            </form>
            <p className={classes.root}>home-page</p>
            <button onClick={handleEmpty} >empty token</button>
        </div>
    );
};

export default Home;