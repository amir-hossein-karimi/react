import { useEffect } from "react"
import { useSelector } from "react-redux"
import { getRouteDetails } from "../../store/ui/uiSelector"

const Layout = ({ children }) => {
    // get current route details
    const { header, title } = useSelector(getRouteDetails)

    useEffect(() => {
        document.title = title
    }, [title])

    return (
        <div>
            {header && 'header'}
            {children}
        </div>
    )
}

export default Layout