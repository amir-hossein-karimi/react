import { useForm as defaultUseForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const useForm = ({ schema, ...options }) => {
  // yup resolver
  const result = defaultUseForm({
    resolver: yupResolver(schema),
    ...options,
  });

  return result;
};

export { useForm, yup };
