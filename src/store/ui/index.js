import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    routeDetails: {}
}

const uiSlice = createSlice({
    name: 'ui',
    initialState,
    reducers: {
        setRouteDetails(state, action) {
            state.routeDetails = action.payload
        }
    }
})

export const { setRouteDetails } = uiSlice.actions
export default uiSlice.reducer