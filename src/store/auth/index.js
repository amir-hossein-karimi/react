import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    access: ""
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setAccess(state, action) {
            state.access = action.payload
        },
        emptyAccess(state) {
            state.access = ""
        }
    }
})

export const { setAccess, emptyAccess } = authSlice.actions
export default authSlice.reducer