// need token
export const innerRoutes = [
    '/'
]

// just when token dont exist
export const privateRoutes = [
    '/login'
]