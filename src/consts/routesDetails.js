

const routeDetails = {
    '/': {
        title: 'home',
        header: true
    },
    '/login': {
        title: 'login',
        header: false
    }
}

export default routeDetails;